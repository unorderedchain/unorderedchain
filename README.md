# UnorderedChain Blockchain

Internal of UnorderedChain, Fabric and FabricCRDT, BIDL, Sync HotStuff: [Link](https://gitlab.com/unorderedchain/unorderedchain/-/tree/main/internal)

Smart contracts for UnorderedChain, Fabric and FabricCRDT, BIDL, Sync HotStuff: [Link](https://gitlab.com/unorderedchain/unorderedchain/-/tree/main/contractsbenchmarks/contracts)

Benchmarking Tool: [Link](https://gitlab.com/unorderedchain/unorderedchain/-/tree/main/internal/benchmark)

Instruction on building: [Link](https://gitlab.com/unorderedchain/unorderedchain/-/blob/main/docs/build.md)


