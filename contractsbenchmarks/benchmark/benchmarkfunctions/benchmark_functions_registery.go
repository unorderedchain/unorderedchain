package benchmarkfunctions

import (
	"errors"
	"strings"
	"unorderedchain/contractsbenchmarks/benchmark/benchmarkfunctions/englishauctioncontractbidl"
	"unorderedchain/contractsbenchmarks/benchmark/benchmarkfunctions/englishauctioncontractfabric"
	"unorderedchain/contractsbenchmarks/benchmark/benchmarkfunctions/englishauctioncontractfabriccrdt"
	"unorderedchain/contractsbenchmarks/benchmark/benchmarkfunctions/englishauctioncontractsynchotstuff"
	"unorderedchain/contractsbenchmarks/benchmark/benchmarkfunctions/englishauctioncontractunorderedchain"
	"unorderedchain/contractsbenchmarks/benchmark/benchmarkfunctions/evotingcontractbidl"
	"unorderedchain/contractsbenchmarks/benchmark/benchmarkfunctions/evotingcontractfabric"
	"unorderedchain/contractsbenchmarks/benchmark/benchmarkfunctions/evotingcontractfabriccrdt"
	"unorderedchain/contractsbenchmarks/benchmark/benchmarkfunctions/evotingcontractsynchotstuff"
	"unorderedchain/contractsbenchmarks/benchmark/benchmarkfunctions/evotingcontractunorderedchain"
	"unorderedchain/contractsbenchmarks/benchmark/benchmarkfunctions/iotcontractunorderedchain"
	"unorderedchain/contractsbenchmarks/benchmark/benchmarkfunctions/syntheticcontractunorderedchain"
	"unorderedchain/internal/contract/contractinterface"
	"unorderedchain/internal/logger"
)

func GetBenchmarkFunctions(contactName string, benchmarkFunctionName string) (contractinterface.BenchmarkFunction, error) {
	contactName = strings.ToLower(contactName)
	benchmarkFunctionName = strings.ToLower(benchmarkFunctionName)
	switch contactName {
	case "syntheticcontractunorderedchain":
		switch benchmarkFunctionName {
		case "readwritetransactionwarm":
			return syntheticcontractunorderedchain.ReadWriteUniformTransactionWarm, nil
		case "read10write90transactionwarm":
			return syntheticcontractunorderedchain.Read10Write90TransactionWarm, nil
		case "read20write80transactionwarm":
			return syntheticcontractunorderedchain.Read20Write80TransactionWarm, nil
		case "read30write70transactionwarm":
			return syntheticcontractunorderedchain.Read30Write70TransactionWarm, nil
		case "read40write60transactionwarm":
			return syntheticcontractunorderedchain.Read40Write60TransactionWarm, nil
		case "read50write50transactionwarm":
			return syntheticcontractunorderedchain.Read50Write50TransactionWarm, nil
		case "read60write40transactionwarm":
			return syntheticcontractunorderedchain.Read60Write40TransactionWarm, nil
		case "read70write30transactionwarm":
			return syntheticcontractunorderedchain.Read70Write30TransactionWarm, nil
		case "read80write20transactionwarm":
			return syntheticcontractunorderedchain.Read80Write20TransactionWarm, nil
		case "read90write10transactionwarm":
			return syntheticcontractunorderedchain.Read90Write10TransactionWarm, nil
		case "readwritetransactioncold":
			return syntheticcontractunorderedchain.ReadWriteUniformTransactionCold, nil
		}
	case "evotingcontractunorderedchain":
		switch benchmarkFunctionName {
		case "getelectionresultswarm":
			return evotingcontractunorderedchain.GetElectionResultsWarm, nil
		case "getelectionresultscold":
			return evotingcontractunorderedchain.GetElectionResultsCold, nil
		case "readwritetransactionwarm":
			return evotingcontractunorderedchain.ReadWriteUniformTransactionWarm, nil
		case "readwritegaussian":
			return evotingcontractunorderedchain.ReadWriteGaussianTransactionWarm, nil
		case "readwritetransactioncold":
			return evotingcontractunorderedchain.ReadWriteUniformTransactionCold, nil
		}
	case "evotingcontractbidl":
		switch benchmarkFunctionName {
		case "initelection":
			return evotingcontractbidl.InitElection, nil
		case "getelectionresults":
			return evotingcontractbidl.GetElectionResults, nil
		case "readwritetransaction":
			return evotingcontractbidl.ReadWriteUniformTransaction, nil
		case "readwritegaussian":
			return evotingcontractbidl.ReadWriteGaussianTransaction, nil
		}
	case "evotingcontractsynchotstuff":
		switch benchmarkFunctionName {
		case "initelection":
			return evotingcontractsynchotstuff.InitElection, nil
		case "getelectionresults":
			return evotingcontractsynchotstuff.GetElectionResults, nil
		case "readwritetransaction":
			return evotingcontractsynchotstuff.ReadWriteUniformTransaction, nil
		case "readwritegaussian":
			return evotingcontractsynchotstuff.ReadWriteGaussianTransaction, nil
		}
	case "evotingcontractfabric":
		switch benchmarkFunctionName {
		case "initelection":
			return evotingcontractfabric.InitElection, nil
		case "getelectionresults":
			return evotingcontractfabric.GetElectionResults, nil
		case "readwritetransaction":
			return evotingcontractfabric.ReadWriteUniformTransaction, nil
		case "readwritegaussian":
			return evotingcontractfabric.ReadWriteGaussianTransaction, nil
		}
	case "evotingcontractfabriccrdt":
		switch benchmarkFunctionName {
		case "initelection":
			return evotingcontractfabriccrdt.InitElection, nil
		case "getelectionresults":
			return evotingcontractfabriccrdt.GetElectionResults, nil
		case "readwritetransaction":
			return evotingcontractfabriccrdt.ReadWriteUniformTransaction, nil
		case "readwritegaussian":
			return evotingcontractfabriccrdt.ReadWriteGaussianTransaction, nil
		}
	case "englishauctioncontractunorderedchain":
		switch benchmarkFunctionName {
		case "getauctionwinnerwarm":
			return englishauctioncontractunorderedchain.GetAuctionWinnerWarm, nil
		case "getauctionwinnercold":
			return englishauctioncontractunorderedchain.GetAuctionWinnerCold, nil
		case "readwritetransactionwarm":
			return englishauctioncontractunorderedchain.ReadWriteUniformTransactionWarm, nil
		case "readwritegaussian":
			return englishauctioncontractunorderedchain.ReadWriteGaussianTransactionWarm, nil
		case "readwritetransactioncold":
			return englishauctioncontractunorderedchain.ReadWriteUniformTransactionCold, nil
		}
	case "englishauctioncontractfabric":
		switch benchmarkFunctionName {
		case "createauction":
			return englishauctioncontractfabric.CreateAuction, nil
		case "getauctionwinner":
			return englishauctioncontractfabric.GetAuctionWinner, nil
		case "readwritetransaction":
			return englishauctioncontractfabric.ReadWriteUniformTransaction, nil
		case "readwritegaussian":
			return englishauctioncontractfabric.ReadWriteGaussianTransaction, nil
		}
	case "englishauctioncontractbidl":
		switch benchmarkFunctionName {
		case "createauction":
			return englishauctioncontractbidl.CreateAuction, nil
		case "getauctionwinner":
			return englishauctioncontractbidl.GetAuctionWinner, nil
		case "readwritetransaction":
			return englishauctioncontractbidl.ReadWriteUniformTransaction, nil
		case "readwritegaussian":
			return englishauctioncontractbidl.ReadWriteGaussianTransaction, nil
		}
	case "englishauctioncontractsynchotstuff":
		switch benchmarkFunctionName {
		case "createauction":
			return englishauctioncontractsynchotstuff.CreateAuction, nil
		case "getauctionwinner":
			return englishauctioncontractsynchotstuff.GetAuctionWinner, nil
		case "readwritetransaction":
			return englishauctioncontractsynchotstuff.ReadWriteUniformTransaction, nil
		case "readwritegaussian":
			return englishauctioncontractsynchotstuff.ReadWriteGaussianTransaction, nil
		}
	case "englishauctioncontractfabriccrdt":
		switch benchmarkFunctionName {
		case "createauction":
			return englishauctioncontractfabriccrdt.CreateAuction, nil
		case "getauctionwinner":
			return englishauctioncontractfabriccrdt.GetAuctionWinner, nil
		case "readwritetransaction":
			return englishauctioncontractfabriccrdt.ReadWriteUniformTransaction, nil
		case "readwritegaussian":
			return englishauctioncontractfabriccrdt.ReadWriteGaussianTransaction, nil
		}
	case "iotcontractunorderedchain":
		switch benchmarkFunctionName {
		case "submitreading":
			return iotcontractunorderedchain.SubmitReading, nil
		case "monitor":
			return iotcontractunorderedchain.Monitor, nil
		case "readwritetransactionwarm":
			return iotcontractunorderedchain.ReadWriteUniformTransactionWarm, nil
		}
	}
	logger.ErrorLogger.Println("smart contract function was not found")
	return nil, errors.New("functions not found")
}
