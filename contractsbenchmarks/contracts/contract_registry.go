package contracts

import (
	"errors"
	"strings"
	"unorderedchain/contractsbenchmarks/contracts/englishauctioncontractbidl"
	"unorderedchain/contractsbenchmarks/contracts/englishauctioncontractfabric"
	"unorderedchain/contractsbenchmarks/contracts/englishauctioncontractfabriccrdt"
	"unorderedchain/contractsbenchmarks/contracts/englishauctioncontractsynchotstuff"
	"unorderedchain/contractsbenchmarks/contracts/englishauctioncontractunorderedchain"
	"unorderedchain/contractsbenchmarks/contracts/evotingcontractbidl"
	"unorderedchain/contractsbenchmarks/contracts/evotingcontractfabric"
	"unorderedchain/contractsbenchmarks/contracts/evotingcontractfabriccrdt"
	"unorderedchain/contractsbenchmarks/contracts/evotingcontractsynchotstuff"
	"unorderedchain/contractsbenchmarks/contracts/evotingcontractunorderedchain"
	"unorderedchain/contractsbenchmarks/contracts/iotcontractunorderedchain"
	"unorderedchain/contractsbenchmarks/contracts/syntheticcontractunorderedchain"
	"unorderedchain/internal/contract/contractinterface"
)

func GetContract(contractName string) (contractinterface.ContractInterface, error) {
	contractName = strings.ToLower(contractName)
	switch contractName {
	case "evotingcontractbidl":
		return evotingcontractbidl.NewContract(), nil
	case "evotingcontractsynchotstuff":
		return evotingcontractsynchotstuff.NewContract(), nil
	case "evotingcontractfabric":
		return evotingcontractfabric.NewContract(), nil
	case "evotingcontractunorderedchain":
		return evotingcontractunorderedchain.NewContract(), nil
	case "evotingcontractfabriccrdt":
		return evotingcontractfabriccrdt.NewContract(), nil
	case "englishauctioncontractfabric":
		return englishauctioncontractfabric.NewContract(), nil
	case "englishauctioncontractbidl":
		return englishauctioncontractbidl.NewContract(), nil
	case "englishauctioncontractsynchotstuff":
		return englishauctioncontractsynchotstuff.NewContract(), nil
	case "englishauctioncontractunorderedchain":
		return englishauctioncontractunorderedchain.NewContract(), nil
	case "englishauctioncontractfabriccrdt":
		return englishauctioncontractfabriccrdt.NewContract(), nil
	case "syntheticcontractunorderedchain":
		return syntheticcontractunorderedchain.NewContract(), nil
	case "iotcontractunorderedchain":
		return iotcontractunorderedchain.NewContract(), nil
	default:
		return nil, errors.New("contract not found")
	}
}

func GetContractNames() []string {
	return []string{
		"englishauctioncontractbidl",
		"englishauctioncontractsynchotstuff",
		"englishauctioncontractunorderedchain",
		"englishauctioncontractfabric",
		"englishauctioncontractfabriccrdt",
		"evotingcontractfabric",
		"evotingcontractfabriccrdt",
		"evotingcontractbidl",
		"evotingcontractsynchotstuff",
		"evotingcontractunorderedchain",
		"syntheticcontractunorderedchain",
		"filestoragecontractunorderedchain",
		"iotcontractunorderedchain",
	}
}
