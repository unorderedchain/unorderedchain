package contractinterface

import (
	"unorderedchain/internal/benchmark/benchmarkutils"
	"unorderedchain/internal/connection/connpool"
	protos "unorderedchain/protos/goprotos"
)

type ContractInterface interface {
	Invoke(ShimInterface, *protos.ProposalRequest) (*protos.ProposalResponse, error)
}

type BaseContractOptions struct {
	Bconfig               *protos.BenchmarkConfig
	BenchmarkFunction     BenchmarkFunction
	BenchmarkUtils        *benchmarkutils.BenchmarkUtils
	CurrentClientPseudoId int
	TotalTransactions     int
	SingleFunctionCounter *SingleFunctionCounter
	NodesConnectionPool   map[string]*connpool.Pool
}
