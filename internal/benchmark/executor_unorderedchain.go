package benchmark

import (
	"context"
	"github.com/golang/protobuf/proto"
	"time"
	"unorderedchain/internal/config"
	"unorderedchain/internal/connection/connpool"
	"unorderedchain/internal/logger"
	"unorderedchain/internal/profiling"
	protos "unorderedchain/protos/goprotos"
)

func (rex *RoundExecutor) executeTransactionPart1UnorderedChain(counter int, startTime time.Time) {
	transactionResult := MakeNewTransactionResultUnorderedChain(counter, rex.endorsementPolicyOrgsWithExtraEndorsement, rex.endorsementPolicyOrgs, rex.signer)
	rex.executor.transactionsResult.lock.Lock()
	rex.executor.transactionsResult.transactions[transactionResult.transaction.TransactionId] = transactionResult
	rex.executor.transactionsResult.lock.Unlock()
	proposal := transactionResult.transaction.MakeProposalRequestBenchmarkExecutor(transactionResult.transactionCounter, rex.baseContractOptions)
	transactionResult.readWriteType = proposal.WriteReadTransaction
	transactionResult.latencyMeasurementInstance = transactionResult.latencyMeasurement(startTime)
	rex.streamProposalUnorderedChain(transactionResult, proposal)
}

func (rex *RoundExecutor) executeTransactionPart2UnorderedChain(tx *TransactionResult) {
	if rex.executor.ShouldFailByzantineNetwork() {
		tx.transaction.Status = protos.TransactionStatus_FAILED_GENERAL
	}
	if tx.transaction.Status == protos.TransactionStatus_FAILED_GENERAL {
		tx.EndTransactionMeasurements()
		rex.executor.makeTransactionDone()
		return
	}
	var commitTransaction *protos.Transaction
	var err error
	if rex.executor.ShouldFailByzantineTampered() {
		commitTransaction, err = tx.transaction.MakeByzantineTransactionBenchmarkExecutorWithClientFullSign(rex.benchmarkConfig, rex.endorsementPolicyOrgs)
	} else {
		commitTransaction, err = tx.transaction.MakeTransactionBenchmarkExecutorWithClientFullSign(rex.benchmarkConfig, rex.endorsementPolicyOrgs)
	}
	if err != nil {
		tx.transaction.Status = protos.TransactionStatus_FAILED_GENERAL
		tx.EndTransactionMeasurements()
		rex.executor.makeTransactionDone()
		return
	}
	rex.streamTransactionUnorderedChain(tx, commitTransaction)
	if tx.transaction.Status == protos.TransactionStatus_FAILED_GENERAL {
		tx.EndTransactionMeasurements()
		rex.executor.makeTransactionDone()
		return
	}
}

func (rex *RoundExecutor) executeTransactionPart3UnorderedChain(tx *TransactionResult) {
	if tx.transaction.Status == protos.TransactionStatus_RUNNING {
		tx.transaction.Status = protos.TransactionStatus_SUCCEEDED
	}
	tx.EndTransactionMeasurements()
	rex.executor.makeTransactionDone()
}

func (rex *RoundExecutor) streamProposalUnorderedChain(transaction *TransactionResult, proposal *protos.ProposalRequest) {
	if profiling.IsBandwidthProfiling {
		transaction.sentProposalBytes = rex.selectedOrgsEndorsementPolicyCount * proto.Size(proposal)
	}
	sentProposals := 0
	for _, nodeId := range rex.selectedOrgsEndorsementPolicy {
		rex.executor.clientProposalStreamLock.RLock()
		streamer, ok := rex.executor.clientProposalStream[nodeId]
		rex.executor.clientProposalStreamLock.RUnlock()
		if !ok {
			continue
		}
		if err := streamer.streamUnorderedChain.Send(proposal); err != nil {
			rex.executor.clientProposalStreamLock.Lock()
			delete(rex.executor.clientProposalStream, nodeId)
			rex.executor.clientProposalStreamLock.Unlock()
			err = rex.executor.makeSingleStreamProposal(nodeId)
			if err != nil {
				logger.ErrorLogger.Println(err)
			}
		}
		sentProposals++

	}
	if sentProposals < rex.endorsementPolicyOrgs {
		transaction.transaction.Status = protos.TransactionStatus_FAILED_GENERAL
	}
}

func (rex *RoundExecutor) streamTransactionUnorderedChain(transactionResult *TransactionResult, transaction *protos.Transaction) {
	if profiling.IsBandwidthProfiling {
		transactionResult.sentTransactionBytes = rex.selectedOrgsEndorsementPolicyCount * proto.Size(transaction)
	}
	sentTransactions := 0
	for _, nodeId := range rex.selectedOrgsEndorsementPolicy {
		rex.executor.clientTransactionStreamLock.RLock()
		streamer, ok := rex.executor.clientTransactionStream[nodeId]
		rex.executor.clientTransactionStreamLock.RUnlock()
		if !ok {
			continue
		}
		if err := streamer.streamUnorderedChain.Send(transaction); err != nil {
			rex.executor.clientTransactionStreamLock.Lock()
			delete(rex.executor.clientTransactionStream, nodeId)
			rex.executor.clientTransactionStreamLock.Unlock()
			err = rex.executor.makeSingleStreamTransactionUnorderedChain(nodeId)
			if err != nil {
				logger.ErrorLogger.Println(err)
			}
		}
		sentTransactions++
	}
	if sentTransactions < rex.endorsementPolicyOrgs {
		transaction.Status = protos.TransactionStatus_FAILED_GENERAL
	}
}

func (ex *Executor) subscriberForProposalEventsUnorderedChain() {
	for node := range ex.nodesConnectionsWatchProposalEvent {
		go func(node string) {
			for {
				conn, err := ex.nodesConnectionsWatchProposalEvent[node].Get(context.Background())
				if conn == nil || err != nil {
					logger.ErrorLogger.Println(err)
					continue
				}
				client := protos.NewTransactionServiceClient(conn.ClientConn)
				stream, err := client.SubscribeProposalResponse(context.Background(), &protos.ProposalResponseEventSubscription{ComponentId: config.Config.UUID})
				if err != nil {
					if errCon := conn.Close(); errCon != nil {
						logger.ErrorLogger.Println(errCon)
					}
					connpool.SleepAndReconnect()
					continue
				}
				for {
					if stream == nil {
						if errCon := conn.Close(); errCon != nil {
							logger.ErrorLogger.Println(errCon)
						}
						break
					}
					proposalResponse, streamErr := stream.Recv()
					if streamErr != nil {
						if errCon := conn.Close(); errCon != nil {
							logger.ErrorLogger.Println(errCon)
						}
						break
					}
					ex.processProposalResponse(proposalResponse)
				}
			}
		}(node)
	}
}

func (ex *Executor) subscriberForNewlyAddedProposalEventsUnorderedChain(newNodes map[string]bool) {
	for node := range newNodes {
		go func(node string) {
			for {
				conn, err := ex.nodesConnectionsWatchProposalEvent[node].Get(context.Background())
				if conn == nil || err != nil {
					logger.ErrorLogger.Println(err)
					continue
				}
				client := protos.NewTransactionServiceClient(conn.ClientConn)
				stream, err := client.SubscribeProposalResponse(context.Background(), &protos.ProposalResponseEventSubscription{ComponentId: config.Config.UUID})
				if err != nil {
					if errCon := conn.Close(); errCon != nil {
						logger.ErrorLogger.Println(errCon)
					}
					connpool.SleepAndReconnect()
					continue
				}
				for {
					if stream == nil {
						if errCon := conn.Close(); errCon != nil {
							logger.ErrorLogger.Println(errCon)
						}
						break
					}
					proposalResponse, streamErr := stream.Recv()
					if streamErr != nil {
						if errCon := conn.Close(); errCon != nil {
							logger.ErrorLogger.Println(errCon)
						}
						break
					}
					ex.processProposalResponse(proposalResponse)
				}
			}
		}(node)
	}
}

func (ex *Executor) processProposalResponse(proposalResponse *protos.ProposalResponse) {
	if ex.roundNotDone {
		readyToSendTransaction := false
		ex.transactionsResult.lock.Lock()
		tx := ex.transactionsResult.transactions[proposalResponse.ProposalId]
		if proposalResponse.Status == protos.ProposalResponse_SUCCESS {
			tx.receivedProposalCount++
		}
		if tx.receivedProposalCount <= tx.receivedProposalExpected {
			tx.transaction.ProposalResponses[proposalResponse.NodeId] = proposalResponse
			if tx.receivedProposalCount == tx.receivedProposalExpected {
				readyToSendTransaction = true
			}
		}
		ex.transactionsResult.lock.Unlock()
		if profiling.IsBandwidthProfiling {
			tx.receivedProposalBytes += proto.Size(proposalResponse)
		}
		if proposalResponse.Status != protos.ProposalResponse_SUCCESS {
			logger.InfoLogger.Println("Transaction failed", protos.TransactionStatus_FAILED_GENERAL)
		}
		if readyToSendTransaction {
			go ex.roundExecutor.executeTransactionPart2UnorderedChain(tx)
		}
	}
}

func (ex *Executor) subscriberForTransactionEventsUnorderedChain() {
	for node := range ex.nodesConnectionsWatchTransactionEvent {
		go func(node string) {
			for {
				conn, err := ex.nodesConnectionsWatchTransactionEvent[node].Get(context.Background())
				if conn == nil || err != nil {
					logger.ErrorLogger.Println(err)
					continue
				}
				client := protos.NewTransactionServiceClient(conn.ClientConn)
				stream, err := client.SubscribeTransactionResponse(context.Background(), &protos.TransactionResponseEventSubscription{
					ComponentId: config.Config.UUID,
					PublicKey:   ex.PublicPrivateKey.PublicKeyString,
				})
				if err != nil {
					if errCon := conn.Close(); errCon != nil {
						logger.ErrorLogger.Println(errCon)
					}
					connpool.SleepAndReconnect()
					continue
				}
				for {
					if stream == nil {
						if errCon := conn.Close(); errCon != nil {
							logger.ErrorLogger.Println(errCon)
						}
						break
					}
					txResponse, streamErr := stream.Recv()
					if streamErr != nil {
						if errCon := conn.Close(); errCon != nil {
							logger.ErrorLogger.Println(errCon)
						}
						break
					}
					ex.processTransactionResponse(txResponse)
				}
			}
		}(node)
	}
}

func (ex *Executor) subscriberForNewlyAddedTransactionEventsUnorderedChain(newNodes map[string]bool) {
	for node := range newNodes {
		go func(node string) {
			for {
				conn, err := ex.nodesConnectionsWatchTransactionEvent[node].Get(context.Background())
				if conn == nil || err != nil {
					logger.ErrorLogger.Println(err)
					continue
				}
				client := protos.NewTransactionServiceClient(conn.ClientConn)
				stream, err := client.SubscribeTransactionResponse(context.Background(), &protos.TransactionResponseEventSubscription{
					ComponentId: config.Config.UUID,
					PublicKey:   ex.PublicPrivateKey.PublicKeyString,
				})
				if err != nil {
					if errCon := conn.Close(); errCon != nil {
						logger.ErrorLogger.Println(errCon)
					}
					connpool.SleepAndReconnect()
					continue
				}
				for {
					if stream == nil {
						if errCon := conn.Close(); errCon != nil {
							logger.ErrorLogger.Println(errCon)
						}
						break
					}
					txResponse, streamErr := stream.Recv()
					if streamErr != nil {
						if errCon := conn.Close(); errCon != nil {
							logger.ErrorLogger.Println(errCon)
						}
						break
					}
					ex.processTransactionResponse(txResponse)
				}
			}
		}(node)
	}
}

func (ex *Executor) processTransactionResponse(txResponse *protos.TransactionResponse) {
	if ex.roundNotDone {
		readyToConcludeTransaction := false
		ex.transactionsResult.lock.Lock()
		tx := ex.transactionsResult.transactions[txResponse.TransactionId]
		if txResponse.Status == protos.TransactionStatus_SUCCEEDED {
			tx.receivedTransactionCommitCount++
		}
		if tx.receivedTransactionCommitCount <= tx.receivedTransactionCommitExpected {
			if tx.receivedTransactionCommitCount == tx.receivedTransactionCommitExpected {
				readyToConcludeTransaction = true
			}
		}
		ex.transactionsResult.lock.Unlock()
		if profiling.IsBandwidthProfiling {
			tx.receivedTransactionBytes += proto.Size(txResponse)
		}
		if readyToConcludeTransaction {
			ex.roundExecutor.executeTransactionPart3UnorderedChain(tx)
		}
	}
}

func (ex *Executor) makeAllStreamTransactionUnorderedChain() {
	for node := range ex.nodesConnectionsStreamTransactions {
		if err := ex.makeSingleStreamTransactionUnorderedChain(node); err != nil {
			logger.ErrorLogger.Println(err)
		}
	}
}

func (ex *Executor) makeSingleStreamTransactionUnorderedChain(node string) error {
	conn, err := ex.nodesConnectionsStreamTransactions[node].Get(context.Background())
	if conn == nil || err != nil {
		logger.ErrorLogger.Println(err)
		return err
	}
	client := protos.NewTransactionServiceClient(conn.ClientConn)
	tempStream := &transactionStream{}
	tempStream.streamUnorderedChain, err = client.CommitUnorderedChainTransactionStream(context.Background())
	if err != nil {
		if errCon := conn.Close(); errCon != nil {
			logger.ErrorLogger.Println(errCon)
		}
		connpool.SleepAndReconnect()
		err = ex.makeSingleStreamTransactionUnorderedChain(node)
		if err != nil {
			logger.ErrorLogger.Println(err)
		}
		return nil
	}
	ex.clientTransactionStreamLock.Lock()
	ex.clientTransactionStream[node] = tempStream
	ex.clientTransactionStreamLock.Unlock()
	return nil
}
